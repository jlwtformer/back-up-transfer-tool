﻿using BUTT.Core.Models;
using BUTT.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUTT.Wpf
{
    public partial class MainWindow : Window
    {
        MainViewModel mvm = new MainViewModel();

        public MainWindow()
        {
            InitializeComponent();

            icSources.ItemsSource = mvm.SourceFolders;
            lbDestinations.ItemsSource = mvm.DestinationDrives;
        }

        private void SortSelectedFolders()
        {
            mvm.SelectedFolders = new List<SourceFolderModel>();

            foreach (SourceFolderModel item in icSources.Items)
            {
                if (item.IsSelected) mvm.SelectedFolders.Add(item);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SortSelectedFolders();
            mvm.SelectedDrive = (DestinationDriveModel)lbDestinations.SelectedItem;

            string message = null;

            foreach (SourceFolderModel folderModel in mvm.SelectedFolders) message += folderModel.SourceName + "\n";

            MessageBox.Show(message);
        }
    }
}
