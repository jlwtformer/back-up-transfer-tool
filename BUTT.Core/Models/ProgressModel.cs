﻿namespace BUTT.Core.Models
{
    public class ProgressModel
    {
        public int Percent { get; set; }
        public string CurrentFolder { get; set; }
        public string CurrentFile { get; set; }
    }
}