﻿using System.IO;

namespace BUTT.Core.Models
{
    public class DestinationDriveModel
    {
        public string DriveName { get; set; }
        public string DrivePath { get; set; }
        public DriveType DriveType { get; set; }
    }
}