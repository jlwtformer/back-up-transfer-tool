﻿namespace BUTT.Core.Models
{
    public class SourceFolderModel
    {
        public bool IsSelected { get; set; }
        public string SourceName { get; set; }
        public string SourcePath { get; set; }
    }
}