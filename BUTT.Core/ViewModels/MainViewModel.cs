﻿using BUTT.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BUTT.Core.ViewModels
{
    public class MainViewModel
    {
        private List<SourceFolderModel> _sourceFolders = new List<SourceFolderModel>();
        private List<SourceFolderModel> _selectedFolders = new List<SourceFolderModel>();
        private List<DestinationDriveModel> destinationDriveModels = new List<DestinationDriveModel>();
        private DestinationDriveModel _selectedDrive;
        private List<string> _transferFiles;

        public MainViewModel()
        {
            GrabSourceFolders();
            GrabDestinationDrives();
        }

        private void GrabSourceFolders()
        {
            string home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string[] dirs = Directory.GetDirectories(home);

            foreach (string dir in dirs)
            {
                SourceFolderModel tempFolder = new SourceFolderModel
                {
                    IsSelected = true,
                    SourceName = dir.Substring(dir.LastIndexOf('\\') + 1),
                    SourcePath = dir
                };

                SourceFolders.Add(tempFolder);
            }
        }

        private void GrabDestinationDrives()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (var drive in drives)
            {
                if (Directory.Exists(drive.RootDirectory.FullName))
                {
                    if (drive.DriveType != DriveType.Fixed)
                    {
                        DestinationDriveModel tempDrive = new DestinationDriveModel
                        {
                            DriveName = drive.Name,
                            DrivePath = drive.RootDirectory.FullName,
                            DriveType = drive.DriveType
                        };

                        DestinationDrives.Add(tempDrive);
                    }
                }
            }
        }

        public void CopyFiles()
        {
            TransferFiles = new List<string>();
            DirectoryInfo drive = new DirectoryInfo(SelectedDrive.DrivePath);

            foreach (SourceFolderModel folderModel in SelectedFolders)
            {
                foreach (var file in drive.GetFiles()) TransferFiles.Add(file.FullName);
            }

            
        }

        public List<SourceFolderModel> SourceFolders
        {
            get { return _sourceFolders; }
            set { _sourceFolders = value; }
        }

        public List<SourceFolderModel> SelectedFolders
        {
            get { return _selectedFolders; }
            set { _selectedFolders = value; }
        }


        public List<DestinationDriveModel> DestinationDrives
        {
            get { return destinationDriveModels; }
            set { destinationDriveModels = value; }
        }

        public DestinationDriveModel SelectedDrive
        {
            get { return _selectedDrive; }
            set { _selectedDrive = value; }
        }


        public List<string> TransferFiles
        {
            get { return _transferFiles; }
            set { _transferFiles = value; }
        }

    }
}